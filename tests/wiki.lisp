;;;; Copyright 2023 Vladimir Sedach <vsedach@common-lisp.net>

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public
;;;; License along with this program. If not, see
;;;; <http://www.gnu.org/licenses/>.

(in-package #:cliki2.tests)

(fiveam:def-suite wiki-tests :in cliki2-tests)
(fiveam:in-suite wiki-tests)

;;; find-article

(fiveam:test find-article-error
  (fiveam:signals (cliki2::cannot-find-article-error)
    (let ((hunchentoot:*request* (make-instance 'mock-request)))
     (cliki2::find-article "NotFound" :error t))))

;;; get-all-articles

(fiveam:test get-all-articles0
  (let* ((tmp-wiki-directory
           (ensure-directories-exist
            (merge-pathnames
             (make-pathname :directory
                            (list :relative
                                  "cliki2test"
                                  (cliki2::make-random-string 10)))
             (uiop:temporary-directory))))
         (cliki2::*wiki*
           (mock 'cliki2::get-directory-lock (constantly nil)
             (cliki2::make-wiki
              "ABC"
              "XYZ"
              tmp-wiki-directory
              "a@b.com"))))
    (fiveam:is
     (equal
      ()
      (cliki2::get-all-articles (constantly t))))))

(fiveam:test get-all-articles1
  (let* ((tmp-wiki-directory
           (ensure-directories-exist
            (merge-pathnames
             (make-pathname :directory
                            (list :relative
                                  "cliki2test"
                                  (cliki2::make-random-string 10)))
             (uiop:temporary-directory))))
         (cliki2::*wiki*
           (mock 'cliki2::get-directory-lock (constantly nil)
             (cliki2::make-wiki
              "ABC"
              "XYZ"
              tmp-wiki-directory
              "a@b.com"))))
    (setf (gethash "article1" (cliki2::articles cliki2::*wiki*))
          (list "article1" ()))

    (fiveam:is
     (equalp
      (list "article1")
      (cliki2::get-all-articles (constantly t))))

    (fiveam:is
     (equalp
      ()
      (cliki2::get-all-articles (constantly nil))))))

(fiveam:test get-all-articles2
  (let* ((tmp-wiki-directory
           (ensure-directories-exist
            (merge-pathnames
             (make-pathname :directory
                            (list :relative
                                  "cliki2test"
                                  (cliki2::make-random-string 10)))
             (uiop:temporary-directory))))
         (cliki2::*wiki*
           (mock 'cliki2::get-directory-lock (constantly nil)
             (cliki2::make-wiki
              "ABC"
              "XYZ"
              tmp-wiki-directory
              "a@b.com"))))
    (setf (gethash "article1" (cliki2::articles cliki2::*wiki*))
          (list "article1" ()))
    (setf (gethash "zarticle1" (cliki2::articles cliki2::*wiki*))
          (list "zarticle1" ()))

    (fiveam:is
     (equalp
      (list "article1" "zarticle1")
      (cliki2::get-all-articles (constantly t))))

    (fiveam:is
     (equalp
      ()
      (cliki2::get-all-articles (constantly nil))))))

;;; file-path

(fiveam:test file-path-article1
  (fiveam:is
   (strong-string=
    (concatenate
     'string
     (namestring (cliki2::home-directory cliki2::*wiki*))
     "articles/abc/article")
    (namestring (cliki2::file-path 'cliki2::article "abc")))))

(fiveam:test file-path-account1
  (fiveam:is
   (strong-string=
    (concatenate
     'string
     (namestring (cliki2::home-directory cliki2::*wiki*))
     "accounts/xyz")
    (namestring (cliki2::file-path 'cliki2::account "xyz")))))

;;; write-to-file

(fiveam:test write-to-file1
  (let ((content (cliki2::make-random-string 50))
        (newfile (merge-pathnames (cliki2::make-random-string 19)
                                  (uiop:temporary-directory))))
    (unwind-protect
         (progn
           (cliki2::write-to-file newfile content)
           (fiveam:is
            (strong-string=
             content
             (alexandria:read-file-into-string newfile))))
      (delete-file newfile))))

;;; wiki-new is tested in other unit test files

;;; update-account is tested in accounts.lisp

;;; update-blacklist is tested in accounts.lisp

;;; record-revision is tested through add-revision in article.lisp

;;; revision-content

(fiveam:test revision-content1
  (let* ((article
           (cliki2::wiki-new
            'cliki2::article
            (cliki2::make-article
            :article-title (cliki2::make-random-string 13))))
         (cliki2::*account*
           (list "Contributor" nil))
         (revision
           (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
             (cliki2::add-revision
              article "Interesting content" "summary"))))
    (fiveam:is
     (strong-string=
      "Interesting content"
      (cliki2::revision-content revision)))))

;;; save-revision-content is tested through record-revision

;;; all-topics

(fiveam:test all-topics1
  (let ((article
          (cliki2::wiki-new
           'cliki2::article
           (cliki2::make-article
            :article-title (cliki2::make-random-string 13))))
        (cliki2::*account*
          (list "Contributor" nil)))

    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
             (cliki2::add-revision
              article "*(Topic1) *(topic2)" "summary"))

    (fiveam:is
     (member "topic1" (cliki2::all-topics) :test 'string=))
    (fiveam:is
     (member "topic2" (cliki2::all-topics) :test 'string=))))

(fiveam:test all-topics-prunes-nonreferenced-topics
  (let ((article
          (cliki2::wiki-new
           'cliki2::article
           (cliki2::make-article
            :article-title (cliki2::make-random-string 13))))
        (cliki2::*account*
          (list "Contributor" nil)))

    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
             (cliki2::add-revision
              article "*(Topic1) *(topic2)" "summary"))

    (setf (gethash "nolongertopic" (cliki2::topic-index cliki2::*wiki*))
          '())

    (fiveam:is
     (eql nil
          (member "nolongertopic" (cliki2::all-topics) :test 'string=)))
    (fiveam:is
     (member "topic1" (cliki2::all-topics) :test 'string=))
    (fiveam:is
     (member "topic2" (cliki2::all-topics) :test 'string=))))

;;; reindex-article

(fiveam:test reindex-article-create
  (let ((article-title (cliki2::make-random-string 15)))
    (cliki2::reindex-article
     article-title "*(topic3) _(linkstome) woooof" "")

    (fiveam:is
     (equalp
      (list article-title)
      (gethash "topic3" (cliki2::topic-index cliki2::*wiki*))))
    (fiveam:is
     (equalp
      (list article-title)
      (gethash "linkstome" (cliki2::backlink-index cliki2::*wiki*))))
    (fiveam:is
     (equalp
      (list article-title)
      (gethash "woooof" (cliki2::search-index cliki2::*wiki*))))))

(fiveam:test reindex-article-create-remove
  (let ((article-title (cliki2::make-random-string 15)))
    (cliki2::reindex-article
     article-title "*(topic4) _(linkstome1) baaark" "")

    (fiveam:is
     (equalp
      (list article-title)
      (gethash "topic4" (cliki2::topic-index cliki2::*wiki*))))
    (fiveam:is
     (equalp
      (list article-title)
      (gethash "linkstome1" (cliki2::backlink-index cliki2::*wiki*))))
    (fiveam:is
     (equalp
      (list article-title)
      (gethash "baaark" (cliki2::search-index cliki2::*wiki*))))

    (cliki2::reindex-article
     article-title "" "*(topic4) _(linkstome1) baaark")

    (fiveam:is
     (equalp
      ()
      (gethash "topic4" (cliki2::topic-index cliki2::*wiki*))))
    (fiveam:is
     (equalp
      ()
      (gethash "linkstome1" (cliki2::backlink-index cliki2::*wiki*))))
    (fiveam:is
     (equalp
      ()
      (gethash "baaark" (cliki2::search-index cliki2::*wiki*))))))

;;; init-recent-changes is only called in load-wiki

;;; read-file is tested through load-wiki in integration tests

;;; load-wiki-article is tested in integration tests

;;; load-wiki is tested in integration tests

;;; make-wiki is tested as fixture for whole unit test suite
