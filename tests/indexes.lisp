;;;; Copyright 2023 Vladimir Sedach <vsedach@common-lisp.net>

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public
;;;; License along with this program. If not, see
;;;; <http://www.gnu.org/licenses/>.

(in-package #:cliki2.tests)

(fiveam:def-suite indexes-tests :in cliki2-tests)
(fiveam:in-suite indexes-tests)

;;; canonicalize

(test-str canonicalize0
  ""
  (cliki2::canonicalize ""))

(test-str canonicalize1
  ""
  (cliki2::canonicalize "     "))

(test-str canonicalize2
  "abc foo"
  (cliki2::canonicalize "  Abc   FOO   "))

;;; collect-links

(fiveam:test collect-links0
  (fiveam:is
   (endp (cliki2::collect-links "\\*" ""))))

(fiveam:test collect-links1
  (fiveam:is
   (endp (cliki2::collect-links "\\*" "abc"))))

(fiveam:test collect-links2
  (fiveam:is
   (equalp
    '("abc")
    (cliki2::collect-links "\\*" "*(abc)"))))

(fiveam:test collect-links3
  (fiveam:is
   (equalp
    '("abc")
    (cliki2::collect-links "\\*" "foobar *(abc) something"))))

(fiveam:test collect-links4
  (fiveam:is
   (equalp
    ()
    (cliki2::collect-links "\\_" "foobar *(abc) something"))))

(fiveam:test collect-links5
  (fiveam:is
   (equalp
    '("abc")
    (cliki2::collect-links "\\*" "foobar *(abc) _(something)"))))

(fiveam:test collect-links6
  (fiveam:is
   (equalp
    '("something")
    (cliki2::collect-links "\\_" "foobar *(abc) _(something)"))))

;;; TODO: fix collect-links
;; (fiveam:test collect-links7
;;   (fiveam:is
;;    (equalp
;;     '("something")
;;     (cliki2::collect-links "\\_" "foobar _() _(something)"))))

;; (fiveam:test collect-links8
;;   (fiveam:is
;;    (equalp
;;     ()
;;     (cliki2::collect-links "\\_" "_()"))))

;; (fiveam:test collect-links9
;;   (fiveam:is
;;    (equalp
;;     ()
;;     (cliki2::collect-links "\\_" "_() _()"))))

;; (fiveam:test collect-links10
;;   (fiveam:is
;;    (equalp
;;     ()
;;     (cliki2::collect-links "\\_" "*() _()"))))

;; (fiveam:test collect-links11
;;   (fiveam:is
;;    (equalp
;;     ()
;;     (cliki2::collect-links "\\*" "*()"))))

;; (fiveam:test collect-links12
;;   (fiveam:is
;;    (equalp
;;     ()
;;     (cliki2::collect-links "\\*" "*() *()"))))

;;; TODO: fix parentheses escaping
;; (fiveam:test collect-links13
;;   (fiveam:is
;;    (equalp
;;     '("foo (bar)")
;;     (cliki2::collect-links "\\_" "_(foo (bar\\))"))))

;;; topics and page-links

(defun word-sets-match? (w1 w2)
  (fiveam:is
   (eql () (set-exclusive-or w1 w2 :test 'strong-string-equal))))

(fiveam:test topics-page-links-random
  (multiple-value-bind (content other-words topics pages)
      (make-random-content)
    (let ((other-words1
            (set-difference
             (set-difference
              (remove "" other-words :test 'equal)
              topics
              :test 'string-equal)
             pages
             :test 'string-equal)))

      (fiveam:is
       (equalp () (intersection other-words1
                                (cliki2::topics content)
                                :test 'string-equal)))

      (fiveam:is
       (equalp () (intersection other-words1
                                (cliki2::page-links content)
                                :test 'string-equal)))

      (word-sets-match? topics (cliki2::topics content))
      (word-sets-match? topics (cliki2::collect-links "\\*" content))
      (word-sets-match? pages (cliki2::page-links content))
      (word-sets-match? pages (cliki2::collect-links "\\_" content)))))

;;; words

(fiveam:test words0
  (fiveam:is
   (equalp () (cliki2::words ""))))

(fiveam:test words1
  (word-sets-match? '("xyz" "page" "differ")
                    (cliki2::words "XYZ Page diffeRence")))

(fiveam:test words-ignore-common-words
  (fiveam:is
   (equalp () (cliki2::words "and but in the"))))

(fiveam:test search-articles0
  (fiveam:is
   (eql () (cliki2::search-articles ""))))

;;; search-articles

(fiveam:test search-articles1
  (let ((word (cliki2::make-random-string 21))
        (article-titles
          (map-into (make-list (1+ (random 10)))
                    (lambda () (cliki2::make-random-string 10)))))
    (setf (gethash (stem:stem (string-downcase word))
                   (cliki2::search-index cliki2::*wiki*))
          (copy-list article-titles))

    (word-sets-match? article-titles (cliki2::search-articles word))))

(fiveam:test search-articles-many
  (let ((words
          (map-into (make-list (+ 2 (random 10)))
                    (lambda () (cliki2::make-random-string 22))))
        (article-titles
          (map-into (make-list (1+ (random 10)))
                    (lambda () (cliki2::make-random-string 17)))))
    (dolist (word words)
      (setf (gethash (stem:stem (string-downcase word))
                     (cliki2::search-index cliki2::*wiki*))
            (copy-list article-titles)))

    (word-sets-match?
     article-titles
     (cliki2::search-articles (format nil "~{~A ~}" words)))))

;;; paginate-article-summaries

(test-str paginate-article-summaries0
  "<ol start=\"1\"></ol>
      <div id=\"paginator\">
      <span>Result page:</span></div>"
  (html-output
    (cliki2::paginate-article-summaries
     "0"
     '())))

(fiveam:test paginate-article-summaries1
  (let ((article
          (cliki2::wiki-new
           'cliki2::article
           (cliki2::make-article :article-title "Paginated 1"))))

    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (cliki2::add-revision
       article
       "why weasels will save us"
       "created page"))

    (fiveam:is
     (equal
      (concatenate 'string
"<ol start=\"1\"><li><a href=\"/Paginated%201\" class=\"internal\">Paginated 1</a> <br />
   why weasels will save us ""
  </li></ol>
      <div id=\"paginator\">
      <span>Result page:</span><span>1</span></div>")
      (html-output
        (cliki2::paginate-article-summaries
         "0"
         '("Paginated 1")))))))

(fiveam:test paginate-article-summaries-2-pages
  (let ((articles
          (map-into
           (make-list 11)
           (lambda ()
            (cliki2::wiki-new
             'cliki2::article
             (cliki2::make-article :article-title (cliki2::make-random-string 17)))))))

    (mapc
     (lambda (article)
       (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
         (cliki2::add-revision
          article
          (cliki2::make-random-string 41)
          "created page")))
     articles)

    (let ((page (html-output
                  (cliki2::paginate-article-summaries
                   "0"
                   (mapcar 'cliki2::article-title articles)))))
      (fiveam:is
       (eql 0 (search "<ol start=\"1\">" page)))

      (fiveam:is
       (cl-ppcre:scan
        "</ol>
      <div id=\"paginator\">
      <span>Result page:</span><span>1</span><span><a href=\"\\?&start=10\">2</a></span><span><a href=\"\\?&start=10\">&gt;</a></span></div>$"
        page))

      (fiveam:is
       (cl-ppcre:scan
        (cl-ppcre:create-scanner
         "<li><a href=\"/[a-zA-Z0-9]{17}\" class=\"internal\">[a-zA-Z0-9]{17}</a> <br />\\s+[a-zA-Z0-9]{41}\\s+</li>"
         :single-line-mode t)
        page)))))

(fiveam:test paginate-article-summaries-3-pages
  (let ((articles
          (map-into
           (make-list 21)
           (lambda ()
            (cliki2::wiki-new
             'cliki2::article
             (cliki2::make-article :article-title (cliki2::make-random-string 17)))))))

    (mapc
     (lambda (article)
       (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
         (cliki2::add-revision
          article
          (cliki2::make-random-string 41)
          "created page")))
     articles)

    (let ((page (html-output
                  (cliki2::paginate-article-summaries
                   "0"
                   (mapcar 'cliki2::article-title articles)))))
      (fiveam:is
       (eql 0 (search "<ol start=\"1\">" page)))

      (fiveam:is
       (cl-ppcre:scan
        "</ol>
      <div id=\"paginator\">
      <span>Result page:</span><span>1</span><span><a href=\"\\?&start=10\">2</a></span><span><a href=\"\\?&start=20\">3</a></span><span><a href=\"\\?&start=10\">&gt;</a></span></div>$"
        page))

      (fiveam:is
       (cl-ppcre:scan
        (cl-ppcre:create-scanner
         "<li><a href=\"/[a-zA-Z0-9]{17}\" class=\"internal\">[a-zA-Z0-9]{17}</a> <br />\\s+[a-zA-Z0-9]{41}\\s+</li>"
         :single-line-mode t)
        page)))))

;;; /site/search

(test-str /site/search0
  (concatenate
   'string
   "<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
    <title>Wooki: Search results</title>
    ""
    <link  rel=\"stylesheet\" href=\"/static/css/style.css\">
    <link  rel=\"stylesheet\" href=\"/static/css/colorize.css\">
  </head>

  <body>
    <span class=\"hidden\">Wooki - Search results</span>
    <div id=\"content\"><div id=\"content-area\"><h1>Search results</h1>No results found</div>
  <div id=\"footer\" class=\"buttonbar\"><ul></ul></div>
  </div>
  <div id=\"header-buttons\" class=\"buttonbar\">
    <ul>
      <li><a href=\"/\">Home</a></li>
      <li><a href=\"/site/recent-changes\">Recent Changes</a></li>
      <li><a href=\"/Wooki\">About</a></li>
      <li><a href=\"/Text%20Formatting\">Text Formatting</a></li>
      <li><a href=\"/site/tools\">Tools</a></li>
    </ul>
    <div id=\"search\">
      <form action=\"/site/search\">
        <label for=\"search_query\" class=\"hidden\">Search Wooki</label>
        <input type=\"text\" name=\"query\" id=\"search_query\" value=\"NoSuchArticle\" />
        <input type=\"submit\" value=\"search\" />
      </form>
    </div>
  </div>
  <div id=\"pageheader\">
    <div id=\"header\">
      <span id=\"logo\">Wooki</span>
      <span id=\"slogan\">test wiki description</span>
      <div id=\"login\"><form method=\"post\" action=\"/site/login\">
                 <label for=\"login_name\" class=\"hidden\">Account name</label>
                 <input type=\"text\" name=\"name\" id=\"login_name\" class=\"login_input\" />
                 <label for= \"login_password\" class=\"hidden\">Password</label>
                 <input type=\"password\" name=\"password\" id=\"login_password\" class=\"login_input\" />
                 <input type=\"submit\" name=\"login\" value=\"login\" id=\"login_submit\" /><br />
                 <div id=\"register\"><a href=\"/site/register\">register</a></div>
                 <input type=\"submit\" name=\"reset-pw\" value=\"reset password\" id=\"reset_pw\" />
               </form>
      </div>
    </div>
  </div>
  </body></html>")
  (get-request 'cliki2::/site/search "query" "NoSuchArticle"))

(fiveam:test /site/search1
  (let ((article
          (cliki2::wiki-new
           'cliki2::article
           (cliki2::make-article :article-title "Paginated 2"))))

    (mock 'hunchentoot:real-remote-addr (constantly "4.5.6.7")
      (cliki2::add-revision
       article
       "why badgers will save us"
       "created page"))

    (fiveam:is
     (equal
      (concatenate 'string
"<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
    <title>Wooki: Search results</title>
    ""
    <link  rel=\"stylesheet\" href=\"/static/css/style.css\">
    <link  rel=\"stylesheet\" href=\"/static/css/colorize.css\">
  </head>

  <body>
    <span class=\"hidden\">Wooki - Search results</span>
    <div id=\"content\"><div id=\"content-area\"><h1>Search results</h1><ol start=\"1\"><li><a href=\"/Paginated%202\" class=\"internal\">Paginated 2</a> <br />
   why badgers will save us ""
  </li></ol>
      <div id=\"paginator\">
      <span>Result page:</span><span>1</span></div></div>
  <div id=\"footer\" class=\"buttonbar\"><ul></ul></div>
  </div>
  <div id=\"header-buttons\" class=\"buttonbar\">
    <ul>
      <li><a href=\"/\">Home</a></li>
      <li><a href=\"/site/recent-changes\">Recent Changes</a></li>
      <li><a href=\"/Wooki\">About</a></li>
      <li><a href=\"/Text%20Formatting\">Text Formatting</a></li>
      <li><a href=\"/site/tools\">Tools</a></li>
    </ul>
    <div id=\"search\">
      <form action=\"/site/search\">
        <label for=\"search_query\" class=\"hidden\">Search Wooki</label>
        <input type=\"text\" name=\"query\" id=\"search_query\" value=\"badger\" />
        <input type=\"submit\" value=\"search\" />
      </form>
    </div>
  </div>
  <div id=\"pageheader\">
    <div id=\"header\">
      <span id=\"logo\">Wooki</span>
      <span id=\"slogan\">test wiki description</span>
      <div id=\"login\"><form method=\"post\" action=\"/site/login\">
                 <label for=\"login_name\" class=\"hidden\">Account name</label>
                 <input type=\"text\" name=\"name\" id=\"login_name\" class=\"login_input\" />
                 <label for= \"login_password\" class=\"hidden\">Password</label>
                 <input type=\"password\" name=\"password\" id=\"login_password\" class=\"login_input\" />
                 <input type=\"submit\" name=\"login\" value=\"login\" id=\"login_submit\" /><br />
                 <div id=\"register\"><a href=\"/site/register\">register</a></div>
                 <input type=\"submit\" name=\"reset-pw\" value=\"reset password\" id=\"reset_pw\" />
               </form>
      </div>
    </div>
  </div>
  </body></html>")
      (get-request 'cliki2::/site/search "query" "badger")))))
