;;;; Copyright 2019, 2023 Vladimir Sedach <vsedach@common-lisp.net>

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; This program is free software: you can redistribute it and/or
;;;; modify it under the terms of the GNU Affero General Public
;;;; License as published by the Free Software Foundation, either
;;;; version 3 of the License, or (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public
;;;; License along with this program. If not, see
;;;; <http://www.gnu.org/licenses/>.

(in-package #:cliki2.tests)

;;;; Mocks

(defparameter *mock-advice-cleanup-list* (list))

(define-condition mock-warning (warning)
  ((mocked-function :initarg :mocked-function
                    :reader mock-warning-mocked-function))
  (:report (lambda (warning s)
             (format s "Mocked function ~A never called"
                     (mock-warning-mocked-function warning)))))

(defmacro mock (func-designator-symbol advice-func &body body)
  (let ((advice  (gensym "advice"))
        (called? (gensym "was-mocked-called?"))
        (args    (gensym "args")))
    `(let* ((,called? nil)
            (,advice (lambda (&rest ,args)
                       (setf ,called? t)
                       (apply ,advice-func ,args))))
       (pushnew ,func-designator-symbol *mock-advice-cleanup-list*)
       (unwind-protect
            (progn
              (handler-bind ((warning 'muffle-warning))
                (cl-advice:add-advice
                 :around ,func-designator-symbol ,advice))
              ,@body)
         (cl-advice:remove-advice
          :around ,func-designator-symbol ,advice)
         (unless ,called?
           (warn 'mock-warning
                 :mocked-function ,func-designator-symbol))))))

(defclass mock-request ()
  ((headers-in     :initarg :headers-in  :initform (list))
   (request-method :initarg :method)
   (parameters     :initarg :parameters  :initform (list))
   (request-uri    :initarg :request-uri :initform "/")))

(defmethod hunchentoot::request-method ((x mock-request))
  (slot-value x 'request-method))

(defmethod hunchentoot::request-uri ((x mock-request))
  (slot-value x 'request-uri))

(defmethod hunchentoot:headers-in ((x mock-request))
  (slot-value x 'headers-in))

(defmethod hunchentoot:remote-addr ((x mock-request))
  "6.6.6.6")

(defmethod hunchentoot:get-parameters ((x mock-request))
  (slot-value x 'parameters))

(defmethod hunchentoot:post-parameters ((x mock-request))
  (slot-value x 'parameters))

(defmethod hunchentoot:cookies-in ((x mock-request))
  (list))

(defvar *redirect-expected?* nil)

(defun fake-request (method handler request-options parameters)
  (let ((hunchentoot:*request*
          (apply 'make-instance
                 'mock-request
                 :method method
                 :parameters (alexandria:plist-alist parameters)
                 request-options))
        redirect-target
        redirect-code)
    (handler-bind ((mock-warning 'muffle-warning))
      (mock 'hunchentoot:redirect
          (lambda (next target &key (code 303) &allow-other-keys)
            (declare (ignore next))
            (unless *redirect-expected?*
              (error "Unexpected redirect"))
            (setf redirect-target target
                  redirect-code code))
        (values (funcall handler) redirect-target redirect-code)))))

(defun get-request (handler &rest params)
  (multiple-value-bind (page redirect response-code)
      (fake-request :get handler () params)
    (unless redirect
      (fiveam:is (plausible-cliki-page? page)))
    (values page redirect response-code)))

(defun post-request (handler &optional opts &rest params)
  (fake-request :post handler opts params))

(defmacro ensure-redirected (request-form target &optional (code 303))
  (let ((a (gensym))
        (b (gensym "expected-redirect-target"))
        (c (gensym "expected-redirect-code")))
   `(let ((*redirect-expected?* t))
      (multiple-value-bind (,a ,b ,c)
         ,request-form
       (declare (ignore ,a))
       (fiveam:is (strong-string= ,target ,b))
       (fiveam:is (= ,code ,c))))))

;;;; Test functions

(defun call-with-test-setup (thunk)
  (let ((tmp-wiki-directory
          (ensure-directories-exist
           (merge-pathnames
            (make-pathname :directory
                           (list :relative
                                 "cliki2test"
                                 (cliki2::make-random-string 10)))
            (uiop:temporary-directory)))))

    (unwind-protect
         (progn
           (enable-contracts)

           (assert (not (boundp 'cliki2::*wiki*)))
           (let ((cliki2::*wiki*
                   (mock 'cliki2::get-directory-lock (constantly nil)
                     (cliki2::make-wiki
                      "Wooki"
                      "test wiki description"
                      tmp-wiki-directory
                      "wiki@domain.com"))))
             (funcall thunk)))

      (disable-contracts)
      (dolist (x *mock-advice-cleanup-list*)
        ;; TODO: debug why sometimes the predicate does not work!
        ;; (when (cl-advice:advisable-function-p x)
        ;;   (cl-advice:make-unadvisable x))
        (handler-case (cl-advice:make-unadvisable x)
          (cl-advice::not-an-advisable-function () nil)))
      (uiop:delete-directory-tree
       tmp-wiki-directory :validate t :if-does-not-exist :ignore))))

(fiveam:def-suite cliki2-tests)

(defun run-tests ()
  (call-with-test-setup
   (lambda ()
     (fiveam:run! 'cliki2-tests))))

(defmacro test-str (name expected actual)
  `(fiveam:test ,name
     (fiveam:is (strong-string= ,expected ,actual))))

(defmacro test-pred (name predicate)
  `(fiveam:test ,name (fiveam:is-true ,predicate)))

(defmacro html-output (&body body)
  ; TODO: the following assertion should be true
  ; (assert (not (boundp 'cliki2::*html-stream*)))
  `(with-output-to-string (cliki2::*html-stream*)
     ,@body))

(defun plausible-cliki-page? (str)
  (eql
   0
   (cl-ppcre:scan
    (cl-ppcre:create-scanner
     "^<!DOCTYPE html>\\s+<html>\\s+<head>\\s+<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\\s+<title>.+</title>.*<link  rel=\"stylesheet\" href=\"/static/css/style.css\">\\s+<link  rel=\"stylesheet\" href=\"/static/css/colorize.css\">\\s+</head>\\s+<body>\\s+<span class=\"hidden\">.+ - .+</span>\\s+<div id=\"content\"><div id=\"content-area\">.*</div>\\s+<div id=\"footer\" class=\"buttonbar\"><ul>.*</ul></div>\\s+</div>\\s+<div id=\"header-buttons\" class=\"buttonbar\">\\s+<ul>\\s+<li><a href=\"/\">Home</a></li>\\s+<li><a href=\"/site/recent-changes\">Recent Changes</a></li>\\s+<li><a href=\"/Wooki\">About</a></li>\\s+<li><a href=\"/Text%20Formatting\">Text Formatting</a></li>\\s+<li><a href=\"/site/tools\">Tools</a></li>\\s+</ul>\\s+<div id=\"search\">\\s+<form action=\"/site/search\">\\s+<label for=\"search_query\" class=\"hidden\">Search .+</label>\\s+<input type=\"text\" name=\"query\" id=\"search_query\" value=\".*\" />\\s+<input type=\"submit\" value=\"search\" />\\s+</form>\\s+</div>\\s+</div>\\s+<div id=\"pageheader\">\\s+<div id=\"header\">\\s+<span id=\"logo\">.+</span>\\s+<span id=\"slogan\">.*</span>\\s+<div id=\"login\">.*</div>\\s+</div>\\s+</div>\\s+</body></html>$"
     :single-line-mode t)
    str)))

(defun write-random-whitespace (stream)
  (dotimes (i (1+ (random 10)))
    (declare (ignorable i))
    (write-char
     (aref #(#\Space #\Tab #\Newline #\Return)
           (random 4))
     stream)))

(defun make-random-content ()
  (flet ((make-words ()
           (map-into (make-list (random 20))
                     (lambda ()
                       (cliki2::make-random-string (random 10)))))

         (write-word (stream bag index fmt)
           (let ((next-word (pop (elt bag index))))
             (when next-word
               (format stream fmt next-word)
               (when (= 1 (random 2))
                 (write-random-whitespace stream))))))

    (let ((words (make-words))
          (topics (make-words))
          (pages (make-words)))

      (values
       (with-output-to-string (s)
         (do ((bag (list (copy-list words)
                         (copy-list topics)
                         (copy-list pages))))
             ((equalp '(() () ()) bag))
           (ecase (random 3)
             (0 (write-word s bag 0 "~A"))
             (1 (write-word s bag 1 "*(~A)"))
             (2 (write-word s bag 2 "_(~A)")))))
       words
       topics
       pages))))
